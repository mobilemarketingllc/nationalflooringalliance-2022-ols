<?php get_header(); ?>

<?php
	global $post;

	$queried_object = get_queried_object();

	$post = get_post( $queried_object->ID );
                    setup_postdata( $post );
                    the_content();
                    wp_reset_postdata( $post );

		$store_shortname       = get_post_meta( $queried_object->ID, 'wpsl_store_shortname', true );
		$address       = get_post_meta( $queried_object->ID, 'wpsl_address', true );
		$city          = get_post_meta( $queried_object->ID, 'wpsl_city', true );
		$zip          = get_post_meta( $queried_object->ID, 'wpsl_zip', true );
		$state          = get_post_meta( $queried_object->ID, 'wpsl_state', true );
		$phone       = get_post_meta( $queried_object->ID, 'wpsl_phone', true );
		$site_url       = get_post_meta( $queried_object->ID, 'wpsl_url', true );
		$url       = get_post_meta( $queried_object->ID, 'wpsl_url', true );
		$country       = get_post_meta( $queried_object->ID, 'wpsl_country', true );
		$destination   = $address . ',' . $city . ',' . $state.',' .$zip;
		$direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "";
		$wpsl_hours = get_post_meta( $queried_object->ID, 'wpsl_hours', true );
		$no_build_site = get_post_meta( $queried_object->ID, 'no_build_site', true );

	?>

<div class="storelocation">
	<div class="header-banner-image-bg">
		<div class="header-banner-image">
		</div>
	</div>
	
	<div class="header-sale-image">		
		
			<?php echo do_shortcode('[coupon "salebanner"]'); ?>	
		
	</div>
	<div class="cta-wrapperMain">
		<div class="container">
			<div class="cta-wrapper">
				<div class="zipcode-wrapper">
				<span class="fl-icon">
				<i class="fas fa-map-marker-alt" aria-hidden="true"></i>
					</span>
				<?php echo do_shortcode("[currentzipcode]"); ?> <a class="usercurent" href="#" data-location="<?php echo do_shortcode("[currentzipcode]"); ?>">Use Current Location</a>
				</div>

				<div class="cta-buttons">
				<div class="fl-button-group-buttons" role="group" aria-label="">
						<div id="fl-button-group-button-6aigsmunhv4l-0" class="fl-button-group-button fl-button-group-button-6aigsmunhv4l-0">
							<div class="fl-button-wrap fl-button-width-full fl-button-right fl-button-has-icon">
							<a href="#" target="_self" class="fl-button" role="button">
									<i class="fl-button-icon fl-button-icon-before dashicons dashicons-before dashicons-format-chat" aria-hidden="true"></i>
										<span class="fl-button-text">CONTACT</span>
									</a>
						</div>
						</div>
						<?php if($site_url != ''){ ?>
						<div id="fl-button-group-button-6aigsmunhv4l-1" class="fl-button-group-button fl-button-group-button-6aigsmunhv4l-1">
							<div class="fl-button-wrap fl-button-width-full fl-button-right fl-button-has-icon">
									<a href="<?php echo $site_url; ?>" target="_blank" class="fl-button" role="button">
											<i class="fl-button-icon fl-button-icon-before far fa-window-restore" aria-hidden="true"></i>
												<span class="fl-button-text">VISIT WEBSITE</span>
											</a>
						</div>
						</div>
						<?php } ?>
						
						<div id="fl-button-group-button-6aigsmunhv4l-2" class="fl-button-group-button fl-button-group-button-6aigsmunhv4l-2">
							<div class="fl-button-wrap fl-button-width-full fl-button-right fl-button-has-icon">
									<a href="<?php echo $direction_url; ?>" target="_blank" class="fl-button" role="button">
											<i class="fl-button-icon fl-button-icon-before fi-map" aria-hidden="true"></i>
												<span class="fl-button-text">GET DIRECTIONS</span>
											</a>
						</div>
						</div>	
					
					</div>

				</div>

			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">	

				<div class="col-lg-5 col-md-6 col-sm-12 storeloc-content">

					<img class="locationIMG" src="<?php echo get_the_post_thumbnail_url( $queried_object->ID); ?>"/>
					
					<span class="city"><?php echo $city;  ?></span>
					<h1 class="entry-title sfnstoretitle"><?php echo get_the_title( $queried_object->ID);  ?></h1>
					
					<?php if(isset($site_url)){?>

					<span class="siteUrl"><a href="https://<?php echo $site_url;?>" target="_blank"><?php echo $site_url;  ?></a></span> 
					
					<?php } ?>

					<span class="nfa-address"><?php echo $destination;?></span>

					<?php if($phone!=''){?>
			
					<span class="phNo">PHONE NUMBER </span>
					<span class="phNoDis"><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></span>

					<?php } ?>
			
					<?php if(isset($wpsl_hours)){ ?>

					<div class="storehours">
						<span>HOURS </span>
						<?php echo do_shortcode( '[wpsl_hours]' ); ?>
					</div>

					<?php } ?>
			
				
					
				</div>

				
				<div class="col-lg-7 col-md-6 col-sm-12 storeloc-map">
					<div class="mapcontainer">						
						
						<div class="store_map_wrap">
														
							<?php echo do_shortcode( '[wpsl_map]' ); ?>

						</div>
										
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="bottomsection">
						<h2>The brands you trust. The local + safe service you deserve.</h2>
						<p>Home is now your everything. Your work. Your school. Your local café. Your gym. When your Home is given a whole new meaning, we're here to help you. We are your local flooring experts that carry all the floor brands you trust. Contact your local flooring store today to shop local and to get started on your next flooring project.
					</p>
					</div>
					
				</div>
			</div>
	</div>
</div>

<?php get_footer(); ?>
